<?php
namespace Dashboard\Model;

use Dashboard\DTO\CampaignActionLogData;
use DateTimeImmutable;
use InvalidArgumentException;
use Exception;

class CampaignActionLogBuilder
{
    private const PART_DELIMITER = ':';
    private const ACTION_NOTHING = [];
    private const ACTION_ADD = ['general' => 'added %1$s', 'value' => ' "%3$s"', 'array' => ' [%3$s]'];
    private const ACTION_EDIT = ['general' => 'edited %1$s', 'value' => ' from "%2$s" to "%3$s"', 'array' => ' added [%3$s], removed [%2$s]'];
    private const ACTION_REMOVE = ['general' => 'removed %1$s', 'value' => ' "%2$s"', 'array' => ' [%2$s]'];

    /** @var int */
    private $campaignId;

    /** @var int */
    private $who;

    /** @var string */
    private $part;

    /** @var string */
    private $name;

    /** @var mixed */
    private $newValue;

    /** @var mixed */
    private $oldValue;

    /** @var bool */
    private $hideValue = false;

    /**
     * @param string $name
     * @param array  $arguments
     *
     * @return CampaignActionLogBuilder
     */
    public function __call(string $name, array $arguments): CampaignActionLogBuilder
    {
        if (!property_exists($this, $name)) {
            throw new InvalidArgumentException(sprintf('Method %s not exists', $name));
        }

        $this->$name = $arguments[0] ?? null;

        return $this;
    }

    /**
     * @return CampaignActionLogBuilder
     */
    public function hideValue(): CampaignActionLogBuilder
    {
        $this->hideValue = true;

        return $this;
    }

    /**
     * @return null|CampaignActionLogData
     *
     * @throws Exception
     */
    public function build(): ?CampaignActionLogData
    {
        $action = $this->chooseAction();
        if (self::ACTION_NOTHING === $action) {
            return null;
        }

        $result = new CampaignActionLogData();
        $result->createdAt = new DateTimeImmutable();
        $result->authorId = $this->who;
        $result->campaignId = $this->campaignId;
        $result->message = $this->buildMessage($action);

        return $result;
    }

    private function buildMessage(array $action): string
    {
        $part = $this->part ? sprintf('%s%s', $this->part, self::PART_DELIMITER) : null;

        return sprintf('%s %s', $part, $this->buildAction($action));
    }

    private function buildAction(array $action): string
    {
        if ($this->areValuesArray()) {
            return $this->buildActionForArrayValues($action);
        }
        $actionFormat = sprintf('%s%s', $action['general'], $this->hideValue ? '' : $action['value']);

        return sprintf($actionFormat, $this->name, $this->oldValue, $this->newValue);
    }

    private function areValuesArray(): bool
    {
        return is_array($this->newValue) || is_array($this->oldValue);
    }

    private function chooseAction(): array
    {
        if ($this->areValuesArray()) {
            return $this->chooseActionForArrayValues();
        }

        if ($this->newValue == $this->oldValue) {
            return self::ACTION_NOTHING;
        }

        if (empty($this->newValue) && !empty($this->oldValue)) {
            return self::ACTION_REMOVE;
        }

        if (!empty($this->newValue) && empty($this->oldValue)) {
            return self::ACTION_ADD;
        }

        if (!empty($this->newValue) && !empty($this->oldValue)) {
            return self::ACTION_EDIT;
        }

        return self::ACTION_NOTHING;
    }

    private function chooseActionForArrayValues(): array
    {
        ['removed' => $removed, 'added' => $added] = $this->getValueArrayDiff();
        $removed = count($removed);
        $added = count($added);

        if ($removed > 0 && $added > 0) {
            return self::ACTION_EDIT;
        }

        if (0 === $added && $removed > 0) {
            return self::ACTION_REMOVE;
        }

        if ($added > 0 && 0 === $removed) {
            return self::ACTION_ADD;
        }

        return self::ACTION_NOTHING;
    }

    private function buildActionForArrayValues(array $action): string
    {
        $actionFormat = sprintf('%s%s', $action['general'], $this->hideValue ? '' : $action['array']);

        ['removed' => $removed, 'added' => $added] = $this->getValueArrayDiff();

        return sprintf($actionFormat, $this->name, implode(', ', $removed), implode(',', $added));
    }

    private function getValueArrayDiff(): array
    {
        $new = (array) $this->newValue;
        $old = (array) $this->oldValue;

        $removed = array_diff($old, $new);
        $added = array_diff($new, $old);

        return [
            'removed' => $removed,
            'added' => $added,
        ];
    }
}
