<?php
namespace AppBundle\Command;

use Omi\Finder\Service\DataPointRefreshService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RefreshDataPointCommand extends ContainerAwareCommand
{
    /** @var DataPointRefreshService */
    private $service;

    protected function configure()
    {
        $this
            ->setName('finder:data-point:refresh')
            ->setDescription('Refresh state of all data points')
        ;
    }

    /**
     * @inheritDoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->service = $this->getContainer()->get('omi.finder.service.data_point_refresh');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->service->refreshDataPoints();
        $output->writeln('Command finished');
    }
}