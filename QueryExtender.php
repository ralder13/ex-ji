<?php
namespace Dashboard\Utils\Query;

use Dashboard\DTO\CalculateSettings;

interface QueryExtender
{
    /**
     * @param string            $query
     * @param CalculateSettings $settings
     *
     * @return string
     */
    public function extend(string $query, CalculateSettings $settings): string;
}
