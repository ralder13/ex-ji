<?php declare(strict_types=1);
namespace Dashboard\Model;

use Dashboard\DTO\ActivityFilterData;
use Dashboard\DTO\DateRangeData;
use DateTimeInterface;
use QueryBuilderBundle\Service\DataPointService;
use EnjoyPanel\QueryDSL\Exception\UndefinedDataPointException;
use Throwable;

class ActivityFilterQueryBuilder
{
    const DATETIME_FORMAT = 'Y-m-d H:i:s';

    /** @var DataPointService */
    private $dataPointService;

    /**
     * @param DataPointService $dataPointService
     */
    public function __construct(DataPointService $dataPointService)
    {
        $this->dataPointService = $dataPointService;
    }

    /**
     * {@inheritdoc}
     */
    public function build(ActivityFilterData $filter): string
    {
        $campaignQueries = [];
        foreach ($filter->campaignIds as $campaignId) {
            $byOneCampaignQueries = [];
            $campaignStatusDataPointName = sprintf('campaign_%d_status', $campaignId);
            if (!$this->isDataPointExists($campaignStatusDataPointName)) {
                continue;
            }

            if (empty($filter->statuses)) {
                $byOneCampaignQueries[] = sprintf('%s is not null', $campaignStatusDataPointName);
            } else {
                $statuses = implode(', ', array_map(function ($status) {
                    return sprintf('\'%s\'', $status);
                }, $filter->statuses));
                $byOneCampaignQueries[] = sprintf('%s is any of %s', $campaignStatusDataPointName, $statuses);
            }

            if ($filter->period instanceof DateRangeData) {
                $byOneCampaignQueries[] = $this->buildLastEventQuery($filter->period->since, $campaignId, '>');
                $byOneCampaignQueries[] = $this->buildLastEventQuery($filter->period->until, $campaignId, '<');
            }

            $byOneCampaignQueries = array_filter($byOneCampaignQueries);

            if (count($byOneCampaignQueries) > 0) {
                $campaignQueries[] = implode(' AND ', $byOneCampaignQueries);
            }
        }

        $campaignQueries = array_map(function ($query) {
            return sprintf('(%s)', $query);
        }, array_filter($campaignQueries));

        return implode(' OR ', $campaignQueries);
    }

    private function buildLastEventQuery(?DateTimeInterface $dateTime, int $campaignId, string $sign): ?string
    {
        if (null === $dateTime) {
            return null;
        }

        $date = $dateTime->format(self::DATETIME_FORMAT);

        return sprintf('campaign_%d_last_event %s \'%s\'', $campaignId, $sign, $date);
    }

    private function isDataPointExists(string $name): bool
    {
        try {
            $this->dataPointService->getByCode($name);

            return true;
        } catch (UndefinedDataPointException $e) {
            return false;
        } catch (Throwable $e) {
            throw $e;
        }
    }
}
