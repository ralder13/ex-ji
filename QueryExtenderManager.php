<?php
namespace Dashboard\Utils\Query;

use Dashboard\DTO\CalculateSettings;
use DateTime;
use DateInterval;
use Dashboard\Service\PanelService;

class QueryExtenderManager implements QueryExtender
{
    /** @var PanelService */
    private $panel;

    /**
     * @param PanelService $panel
     */
    public function __construct(PanelService $panel)
    {
        $this->panel = $panel;
    }

    /**
     * {@inheritdoc}
     */
    public function extend(string $query, CalculateSettings $settings): string
    {
        $extendedQuery = $query;

        $panelsConditions = [];
        foreach ($settings->panels as $panelId) {
            $panel = $this->panel->get($panelId);

            $panelCondition = [];
            if (!empty($panel->includeCondition)) {
                $panelCondition[] = $panel->includeCondition;
            }
            if ($settings->onlyValidMembers && !empty($panel->validCondition)) {
                $panelCondition[] = $panel->validCondition;
            }
            if (null !== $settings->memberLastActiveMonths && null !== $panel->dataPointLastActive) {
                $date = (new DateTime())->sub(DateInterval::createFromDateString(sprintf('%d months', $settings->memberLastActiveMonths)));
                $panelCondition[] = sprintf('%s >= %s', $panel->dataPointLastActive, $date->format('Y-m-d'));
            }

            if (count($panelCondition)) {
                $panelsConditions[] = $this->joinConditionsByOperator($panelCondition, 'AND');
            }
        }

        if (count($panelsConditions)) {
            $panelsConditionsQuery = implode(' OR ', $panelsConditions);
            if (empty($extendedQuery)) {
                $extendedQuery = $panelsConditionsQuery;
            } else {
                $extendedQuery = sprintf('(%s) AND (%s)', $extendedQuery, $panelsConditionsQuery);
            }
        }

        return $extendedQuery;
    }

    /**
     * @param string[] $conditions
     * @param string   $operator
     *
     * @return string
     */
    private function joinConditionsByOperator(array $conditions, string $operator): string
    {
        $format = count($conditions) > 1 ? '(%s)' : '%s';
        $operator = sprintf(' %s ', $operator);

        return sprintf($format, implode($operator, array_map(function ($condition) {
            return sprintf('(%s)', $condition);
        }, $conditions)));
    }
}
