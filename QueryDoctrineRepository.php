<?php
namespace Omi\Finder\Repository\Doctrine;

use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\ResultStatement;
use EnjoyFinder\ApiBundle\DTO\QueryData;
use Omi\Finder\Repository\QueryRepository;
use PDO;
use Vilks\Doctrine\DBAL\Type\IntArrayType;
use Vilks\Doctrine\DBAL\Type\TimestampType;
use Vilks\Doctrine\Repository\DoctrineRepository;

class QueryDoctrineRepository extends DoctrineRepository implements QueryRepository
{
    const TABLE = 'query';

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        $row = $this->getQueryBuilder()
            ->select(['q.query_id as id', 'q.result_length', 'q.query as "query"', 'q.status as "status"'])
            ->from(self::TABLE, 'q')
            ->where('query_id = :id')
            ->setParameter('id', $id)
            ->execute()
            ->fetch(PDO::FETCH_ASSOC);

        return $row ? $this->buildData($row) : null;
    }

    /**
     * @inheritDoc
     */
    public function findByIds(array $ids): array
    {
        if (!count($ids)) {
            return [];
        }
        /** @var ResultStatement $stmt */
        $stmt = $this->getQueryBuilder()
            ->select(['q.query_id as id', 'q.result_length', 'q.query as "query"', 'q.status as "status"'])
            ->from(self::TABLE, 'q')
            ->where('q.query_id IN (:ids)')
            ->setParameter('ids', $ids, Connection::PARAM_INT_ARRAY)
            ->execute();

        $list = [];
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $data = $this->buildData($row);
            $list[$data->id] = $data;
        }

        return $list;
    }

    /**
     * @inheritDoc
     */
    public function create(QueryData $data)
    {
        $this->getConnection()->insert(
            self::TABLE,
            ['query' => ($data->query ?: '') , 'status' => QueryData::STATUS_WAITING]
        );

        return $this->getConnection()->lastInsertId(sprintf('%s_query_id_seq', self::TABLE));
    }

    /**
     * @inheritDoc
     */
    public function update(QueryData $data)
    {
        $this->getConnection()->update(self::TABLE, ['query' => $data->query], ['query_id' => $data->id]);
    }

    /**
     * @inheritDoc
     */
    public function remove(int $id)
    {
        $this->getConnection()->delete(self::TABLE, ['query_id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function updateStatus(int $id, string $status)
    {
        $this->getConnection()->update(self::TABLE, ['status' => $status], ['query_id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function updateStatusIf(int $id, string $newStatus, string $expectedStatus): bool
    {
        return (bool)$this->getConnection()->update(
            self::TABLE,
            ['status' => $newStatus],
            [
                'query_id' => $id,
                'status' => $expectedStatus,
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function slice(int $id, int $offset, int $limit): array
    {
        /** @var ResultStatement $stmt */
        $stmt = $this->getQueryBuilder()
            ->select(sprintf('q.result_members[%d:%d] as members', $offset + 1, $offset + $limit))
            ->from(self::TABLE, 'q')
            ->where('q.query_id = :id')
            ->setParameter('id', $id)
            ->execute();

        return $this->convertToPhp(IntArrayType::NAME, $stmt->fetchColumn());
    }

    /**
     * @inheritDoc
     */
    public function writeQuery(int $queryId, array $result)
    {
        $this->getConnection()->update(
            self::TABLE,
            [
                'result_members' => $result,
                'result_length' => count($result),
                'executed_at' => new DateTime(),
            ],
            ['query_id' => $queryId],
            [
                'result_members' => IntArrayType::NAME,
                'result_length' => PDO::PARAM_INT,
                'executed_at' => TimestampType::NAME,
            ]
        );
    }

    /**
     * @param array $row
     *
     * @return QueryData
     */
    private function buildData(array $row)
    {
        $data = new QueryData();
        $data->id = $row['id'];
        $data->count = $row['result_length'];
        $data->status = $row['status'];
        $data->query = $row['query'];

        return $data;
    }
}
