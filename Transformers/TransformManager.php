<?php
namespace EnjoyPanel\DataPoint\Transformers;

use EnjoyPanel\DataPoint\DTO\DataPointData;

class TransformManager implements TransformService
{
    /** @var TransformerInterface[] */
    private $transformers = [];

    /**
     * @param TransformerInterface[] $transformers
     */
    public function __construct(array $transformers)
    {
        foreach ($transformers as $transformer) {
            $this->add($transformer);
        }
    }

    /**
     * @param TransformerInterface $transformer
     */
    public function add(TransformerInterface $transformer)
    {
        $this->transformers[] = $transformer;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value, DataPointData $point)
    {
        foreach ($this->transformers as $transformer) {
            if ($transformer->support($point)) {
                $value = $transformer->transform($value, $point);
            }
        }

        return $value;
    }
}
