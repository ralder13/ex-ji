<?php
namespace EnjoyPanel\DataPoint\Transformers;

use EnjoyPanel\DataPoint\DTO\DataPointData;
use EnjoyPanel\Storage\Service\StorageService;

class UniqueStringTransformer implements TransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function support(DataPointData $point): bool
    {
        return DataPointData::TYPE_STRING === $point->type &&
            StorageService::UNIQUE_STORAGE === $point->storage;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value, DataPointData $point)
    {
        return is_string($value) ? mb_strtolower($value) : $value;
    }
}
