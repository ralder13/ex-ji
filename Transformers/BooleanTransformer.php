<?php
namespace EnjoyPanel\DataPoint\Transformers;

use EnjoyPanel\DataPoint\DTO\DataPointData;

class BooleanTransformer implements TransformerInterface
{
    const MAP_VALUES = [
        '0' => false,
        '1' => true,
        'true' => true,
        'false' => false,
        't' => true,
        'f' => false,
        'yes' => true,
        'no' => false,
    ];

    /**
     * {@inheritdoc}
     */
    public function support(DataPointData $point): bool
    {
        return DataPointData::TYPE_BOOLEAN === $point->type;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value, DataPointData $point)
    {
        return self::MAP_VALUES[$value] ?? $value;
    }
}
