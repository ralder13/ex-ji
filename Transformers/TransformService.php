<?php
namespace EnjoyPanel\DataPoint\Transformers;

use EnjoyPanel\DataPoint\DTO\DataPointData;

interface TransformService
{
    /**
     * @param mixed         $value
     * @param DataPointData $point
     *
     * @return mixed
     */
    public function transform($value, DataPointData $point);
}
