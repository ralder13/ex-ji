<?php
namespace EnjoyPanel\DataPoint\Transformers;

use EnjoyPanel\DataPoint\DTO\DataPointData;

class StringTransformer implements TransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function support(DataPointData $point): bool
    {
        return DataPointData::TYPE_STRING === $point->type;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value, DataPointData $point)
    {
        return is_string($value) ? trim($value) : $value;
    }
}
