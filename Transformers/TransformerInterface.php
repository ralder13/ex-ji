<?php
namespace EnjoyPanel\DataPoint\Transformers;

use EnjoyPanel\DataPoint\DTO\DataPointData;

interface TransformerInterface
{
    /**
     * @param mixed         $value
     * @param DataPointData $point
     *
     * @return mixed
     */
    public function transform($value, DataPointData $point);

    /**
     * @param DataPointData $point
     *
     * @return bool
     */
    public function support(DataPointData $point): bool;
}
